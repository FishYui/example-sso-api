/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

    'POST /user': 'admin/signup-one',
    'POST /users': 'admin/signup-more',
    'DELETE /user/:id': 'admin/delete',
    'GET /users': 'admin/find-all',
    'GET /user/:id': 'admin/find-one',
    'PUT /user/:id': 'admin/update',
    
    'POST /user/signup': 'user/signup',
    'POST /user/login': 'user/login',
    'POST /user/logout': 'user/logout',
    'POST /user/me': 'user/detail',
    'PUT /user/me': 'user/update',
    'DELETE /user/me': 'user/delete',
    'POST /user/resetPassword': 'user/forget-password',
    'PUT /user/resetPassword': 'user/reset-password',
    
}
