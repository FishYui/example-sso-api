/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/

  // '*': true,
  'user/login': true,
  'user/signup': true,
  'user/forget-password': true,
  'user/reset-password': true,
  '*': 'auth',
  'admin/delete': ['auth', 'isAdmin'],
  'admin/find-all': ['auth', 'isAdmin'],
  'admin/find-one': ['auth', 'isAdmin'],
  'admin/update': ['auth', 'isAdmin'],
  'admin/signup-more': ['auth', 'isAdmin'],
  'admin/signup-one': ['auth', 'isAdmin'],
};
