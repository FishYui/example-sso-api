module.exports.errcode = {

  code: {

    //登錄錯誤
    'LOGIN-0001': {
      status: 400,
      message: '查無使用者名稱。'
    },
    'LOGIN-0002': {
      status: 400,
      message: '密碼輸入錯誤。'
    },


    //身分驗證錯誤
    'AUTH-0001': {
      status: 400,
      message: '未加上 Authorization 標頭。'
    },
    'AUTH-0002': {
      status: 400,
      message: 'Authorization Token Undefined。'
    },
    'AUTH-0003': {
      status: 400,
      message: 'Authorization Token 過期。'
    },
    'AUTH-0004': {
      status: 400,
      message: '您所屬的身分組不允許執行此動作。'
    },


    //註冊錯誤
    'SIGNUP-0001': {
      status: 400,
      message: '註冊資料輸入錯誤。'
    },
    'SIGNUP-0002': {
      status: 400,
      message: '使用者名稱已被使用。'
    },
    'SIGNUP-0003': {
      status: 400,
      message: '學號已被使用。'
    },


    //傳送Email錯誤
    'EMAIL-0001': {
      status: 500,
      message: '信件送出失敗。'
    },


    //使用者管理錯誤
    'USER-0001': {
      status: 500,
      message: '查無使用者資訊。'
    },
    'USER-0002': {
      status: 400,
      message: '輸入的修改資料錯誤。'
    }
  }
}