module.exports = {

  friendlyName: 'Detail',

  description: 'See selt Detail',

  inputs: {

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const userId = this.req.thisUser.userId;

    const findUser = await Users.findOne({
      where: { id: userId },
      select: ['id', 'username', 'studentId', 'role', 'name', 'mail', 'createdAt', 'updatedAt']
    });

    if (!findUser) {
      return exits.err('USER-0001');
    }

    return exits.success(findUser);
  }
}