module.exports = {

  friendlyName: 'LogIn',

  description: 'User LogIn System.',

  inputs: {

    username: {
      type: 'string',
      required: true
    },
    password: {
      type: 'string',
      required: true
    }

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const { username, password } = inputs

    const findUser = await Users.findOne({
      where: { username: username }
    }).decrypt()

    if (!findUser) {
      return exits.err('LOGIN-0001')
    }

    if (!(password === findUser.password)) {
      return exits.err('LOGIN-0002')
    }

    const createSession = await Sessions.create({ user: findUser.id })
      .intercept(() => {
        return exits.err('Other Error')
      }).fetch()

    const data = {
      token: createSession.token,
      expiredAt: createSession.expiredAt,
      id: createSession.user
    }

    return exits.success(data)
  }
}
