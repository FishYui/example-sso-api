module.exports = {

  friendlyName: 'Update',

  description: 'Update self Data.',

  inputs: {

    password: {
      type: 'string',
      required: false
    },
    studentId: {
      type: 'string',
      required: false
    },
    role: {
      type: 'ref',
      required: false
    },
    name: {
      type: 'string',
      required: false
    },
    mail: {
      type: 'string',
      required: false
    }

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const userId = this.req.thisUser.userId;
    const userData = {} = inputs;

    const updateUser = await Users.updateOne({
      where: { id: userId }
    })
      .set(userData)
      .intercept({ name: 'UsageError', code: 'E_INVALID_VALUES_TO_SET' }, () => {
        return exits.err('USER-0002');;
      })
      .intercept(() => {
        return exits.err('Other Err');
      });

    if (!updateUser) {
      return exits.err('USER-0001');
    }

    delete updateUser.password;

    return exits.success(updateUser);
  }
}