module.exports = {

  friendlyName: 'Forget PassWord',

  description: 'User Forget self PassWord.',

  inputs: {

    username: {
      type: 'string',
      required: true
    },
    studentId: {
      type: 'string',
      required: true
    }

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const { username, studentId } = inputs;

    const findUser = await Users.findOne({
      where: { username: username, studentId: studentId }
    });

    if (!findUser) {
      return exits.err('USER-0001');
    }

    // 寄信
    const option = {
      to: findUser.mail,
      subject: 'SSO API 重設密碼通知',
      html: `請至以下網址重設密碼: <a href=http://localhost/resetPage/${findUser.id}>http://localhost/resetPage/${findUser.id}</a>`
    };
    const result = await sails.helpers.systemEmail(option);

    if(!result){
      return exits.err('EMAIL-0001');
    }

    return exits.success();
  }
}