module.exports = {

  friendlyName: 'Delete',

  description: 'Delete self Data.',

  inputs: {

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const userId = this.req.thisUser.userId;

    const deleteUser = await Users.destroyOne({
      where: { id: userId }
    })
      .meta({ cascade: false });

    if(!deleteUser){
      return exits.err('USER-0001');
    }
  
    delete deleteUser.password;

    return exits.success(deleteUser);
  }
}