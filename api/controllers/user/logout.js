module.exports = {

  friendlyName: 'LogOut',

  description: 'User LogOut.',

  inputs: {

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const sessionId = this.req.thisUser.sessionId;

    const deleteSession = await Sessions.destroyOne({
      where: { id: sessionId }
    });

    return exits.success();
  }
}