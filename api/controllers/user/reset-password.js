module.exports = {

  friendlyName: 'Reset PassWord',

  description: 'User Reset self PassWord.',

  inputs: {

    sequenceId: {
      type: 'string',
      required: true
    },
    password: {
      type: 'string',
      required: true
    }

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const { sequenceId, password } = inputs;

    const resetPassWord = await Users.updateOne({
      where: { id: sequenceId }
    })
      .set({password})
      .intercept({ name: 'UsageError', code: 'E_INVALID_VALUES_TO_SET' }, () => {
        return exits.err('USER-0002');
      })
      .intercept(() => {
        return exits.err('Other Err');
      });

    if (!resetPassWord) {
      return exits.err('USER-0001');
    }

    return exits.success();
  }
}