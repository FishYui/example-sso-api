//const nodemailer = require('nodemailer');

module.exports = {

  friendlyName: 'SignUp',

  description: 'User SignUp.',

  inputs: {

    username: {
      type: 'string',
      required: true
    },
    password: {
      type: 'string',
      required: true
    },
    name: {
      type: 'string',
      required: true
    },
    mail: {
      type: 'string',
      required: true
    }

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const inp = {} = inputs

    const date = new Date()
    const year = date.getFullYear() - 1911
    const month = date.getMonth()
    const schoolYear = month > 7 ? year : year - 1

    const lastUser = await Users.find({
      where: { studentId: { startsWith: schoolYear } },
      limit: 1,
      sort: 'id DESC',
      select: ['studentId']
    })

    let studentId
    if (lastUser.length === 0) {
      studentId = `${schoolYear}0001`
    } else {
      const lastUserId = parseInt(lastUser[0].studentId)
      studentId = (lastUserId + 1).toString()
    }

    inp.studentId = studentId

    const createUser = await Users.create(inp)
      .intercept({ name: 'UsageError', code: 'E_INVALID_NEW_RECORD' }, () => {
        return exits.err('SIGNUP-0001')
      })
      .intercept({ name: 'AdapterError', code: 'E_UNIQUE' }, (err) => {
        if (err.attrNames[0] === 'username') {
          return exits.err('SIGNUP-0002')
        } else if (err.attrNames[0] === 'studentId') {
          return exits.err('SIGNUP-0003')
        }
      })
      .intercept(() => {
        return exits.err('Other Error')
      })
      .fetch()
    delete createUser.password

    return exits.success(createUser)
  }
}
