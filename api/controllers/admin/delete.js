module.exports = {

  friendlyName: 'Delete',

  description: 'Delete one User.',

  inputs: {

    id: {
      type: 'number',
      required: true
    }

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const userId = inputs.id;

    const deleteUser = await Users.destroyOne({
      where: { id: userId }
    }).meta({ cascade: false })
      .intercept(() => {
        return exits.err('Other Err');
      });

    if (!deleteUser) {
      return exits.err('USER-0001');
    }

    return exits.success();
  }
}