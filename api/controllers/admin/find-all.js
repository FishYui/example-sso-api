module.exports = {

  friendlyName: 'Find',

  description: 'Find all Users.',

  inputs: {

    page: {
      type: 'number',
      required: false,
      defaultsTo: 1
    },
    pageSize: {
      type: 'number',
      required: false,
      defaultsTo: 10
    },
    sort: {
      type: 'string',
      required: false,
      defaultsTo: 'createdAt'
    },
    desc: {
      type: 'boolean',
      required: false,
      defaultsTo: false
    }

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const { page, pageSize, sort, desc } = inputs;

    const findUser = await Users.find({
      limit: pageSize > 0 ? pageSize : 10,
      skip: page >= 1 ? (page - 1) * pageSize : 0,
      sort: `${sort} ${desc === true ? 'DESC' : 'ASC'}`
    })
      .intercept(() => {
        return exits.err('Other Error')
      });

    return exits.success(findUser);
  }
}