module.exports = {

  friendlyName: 'Find',

  description: 'Find one User.',

  inputs: {

    id: {
      type: 'number',
      required: true
    }

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const userId = inputs.id;

    const findUser = await Users.findOne({
      where: { id: userId }
    })
      .intercept(() => {
        return exits.err('Other Error')
      });

    if (!findUser) {
      return exits.err('USER-0001');
    }

    delete findUser.password;

    return exits.success(findUser);
  }
}