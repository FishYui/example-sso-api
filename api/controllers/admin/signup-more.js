module.exports = {

  friendlyName: 'SignUp',

  description: 'Admin SignUp more User.',

  inputs: {

    jsonData: {
      type: 'json',
      require: true
    }

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const { jsonData } = inputs;
    let error = [];

    for (const data of jsonData) {

      const password = Math.random().toString(36).substr(2);
      data.password = password;

      const createUser = await Users.create(data)
        .tolerate({ name: 'UsageError', code: 'E_INVALID_NEW_RECORD' }, () => {
          error.push({ username: data.username, message: '註冊資料輸入錯誤。' });
        })
        .tolerate({ name: 'AdapterError', code: 'E_UNIQUE' }, (err) => {
          if (err.attrNames[0] === 'username') {
            error.push({ username: data.username, message: '使用者名稱已被使用。' });
          } else if (err.attrNames[0] === 'studentId') {
            error.push({ username: data.username, message: '學號已被使用。' });
          }
        })
        .tolerate(() => {
          error.push({ username: data.username, message: 'Other Error。' });
        })
        .fetch();

      // 寄信
      if (createUser) {

        const option = {
          to: createUser.mail,
          subject: 'SSO API 註冊通知',
          html: `請至以下網址重設密碼: <a href=http://localhost/resetPage/${createUser.id}>http://localhost/resetPage/${createUser.id}</a>`
        };
        const result = await sails.helpers.systemEmail(option);

        if (!result) {
          Users.destroyOne({
            where: { id: createUser.id }
          });
          return exits.err('EMAIL-0001');
        }
      }
    }

    if (error.length != 0) {
      return this.res.status(400).json({ success: false, error: error });
    }

    return exits.success();
  }
}
