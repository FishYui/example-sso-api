module.exports = {

  friendlyName: 'SignUp',

  description: 'Admin SignUp one User.',

  inputs: {

    username: {
      type: 'string',
      required: true
    },
    studentId: {
      type: 'string',
      required: true
    },
    role: {
      type: 'ref',
      required: true
    },
    name: {
      type: 'string',
      required: true
    },
    mail: {
      type: 'string',
      required: true
    }

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const inp = {} = inputs

    const password = Math.random().toString(36).substr(2)
    inp.password = password

    const createUser = await Users.create(inp)
      .intercept({ name: 'UsageError', code: 'E_INVALID_NEW_RECORD' }, () => {
        return exits.err('SIGNUP-0001')
      })
      .intercept({ name: 'AdapterError', code: 'E_UNIQUE' }, (err) => {
        if (err.attrNames[0] === 'username') {
          return exits.err('SIGNUP-0002')
        } else if (err.attrNames[0] === 'studentId') {
          return exits.err('SIGNUP-0003')
        }
      })
      .intercept(() => {
        return exits.err('Other Error')
      })
      .fetch()
    delete createUser.password

    // 寄信
    const option = {
      to: createUser.mail,
      subject: 'SSO API 註冊通知',
      html: `請至以下網址重設密碼: <a href=http://localhost/resetPage/${createUser.id}>http://localhost/resetPage/${createUser.id}</a>`
    };
    const result = await sails.helpers.systemEmail(option);

    if(!result){
      Users.destroyOne({
        where: { id: createUser.id }
      });
      return exits.err('EMAIL-0001');
    }

    return exits.success(createUser);
  }
}
