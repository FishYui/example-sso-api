module.exports = async function (req, res, next) {

  const role = req.thisUser.role;

  if (role.includes(999)) {

    req.thisUser.checkRole = true;
    
    return next();
  }

  return res.err('AUTH-0004');
}