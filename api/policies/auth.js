module.exports = async function (req, res, next) {

  const authorization = req.headers.authorization;

  if (authorization) {

    const [authType, authToken] = authorization.split(' ');

    if (authType === 'Bearer' && authToken) {

      const findSession = await Sessions.findOne({
        where: { token: authToken }
      })
        .populate('user')
        .intercept(() => {
          return res.err('Other Error')
        });

      if (!findSession) {
        return res.err('AUTH-0002');
      }

      if (Date.now() > findSession.expiredAt) {
        return res.err('AUTH-0003');
      }

      req.thisUser = {
        userId: findSession.user.id,
        sessionId: findSession.id,
        role: findSession.user.role
      }

      return next();
    }
  }

  return res.err('AUTH-0001');
}