const nodemailer = require('nodemailer');

module.exports = {

  friendlyName: 'Email',

  description: 'Send System Email.',

  inputs: {

    option: {
      type: 'ref',
      required: true
    }

  },

  exits: {

  },

  fn: async function (inputs, exits) {

    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        type: 'OAuth2',
        user: sails.config.emailServer.account,
        clientId: sails.config.emailServer.clientId,
        clientSecret: sails.config.emailServer.clientSecret,
        refreshToken: sails.config.emailServer.refreshToken,
      }
    });

    const mailOptions = inputs.option;
    mailOptions.from = sails.config.emailServer.account;

    try {

      await transporter.sendMail(mailOptions);
      return exits.success(true);

    } catch (err) {
      return exits.success(false);
    }
  }
}