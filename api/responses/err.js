const e = sails.config.errcode.code;

module.exports = function err(errCode) {

    const req = this.req;
    const res = this.res;

    if (errCode === undefined || e[errCode] === undefined) {
        sails.log.info('Other Error');
        return res.status(400).json({ success: false, message: 'Other Error' });
    }

    const resData = {
        status: e[errCode].status,
        message: e[errCode].message
    };

    return res.status(resData.status).json({
        success: false,
        message: resData.message
    });

}
