module.exports = {

    attributes: {

        username: {
            type: 'string',
            required: true,
            columnType: 'VARCHAR (20)',
            unique: true,
            maxLength: 20
        },
        password: {
            type: 'string',
            required: true,
            encrypt: true
        },
        studentId: {
            type: 'string',
            required: true,
            columnType: 'VARCHAR (7)',
            unique: true,
            maxLength: 7
        },
        role: { // 0 管理員、1 老師、999 學生；預設為 [999]
            type: 'ref',
            columnType: 'integer[]',
            defaultsTo: [999]
        },
        name: {
            type: 'string',
            required: true,
            columnType: 'VARCHAR (20)',
            maxLength: 20
        },
        mail: {
            type: 'string',
            required: true,
            isEmail: true
        },

        sessions: {
            collection: 'Sessions',
            via: 'user'
        },
        apps: {
            collection: 'Apps',
            via: 'owner'
        }
    }
}