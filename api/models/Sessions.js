const uuid = require('uuid/v4');

module.exports = {

    attributes: {

        user: {
            model: 'Users',
            required: true
        },

        token: {
            type: 'string'
        },
        expiredAt: {
            type: 'ref',
            columnType: 'bigInt'
        }
    },

    beforeCreate: function (data, proceed) {
        
        data.token = uuid();

        const datetime = Date.now();
        data.expiredAt = datetime + 10800000;

        return proceed();
    },

    customToJSON: function () {

        this.expiredAt = parseInt(this.expiredAt);

        return this;
    }
}