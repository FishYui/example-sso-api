const uuid = require('uuid/v4');

module.exports = {

    attributes: {

        owner: {
            model: 'Users',
            required: true
        },
        name: {
            type: 'string',
            required: true,
            columnType: 'VARCHAR (20)',
            maxLength: 20
        },
        apiSecret: {
            type: 'string'
        }
    },

    beforeCreate: function(data, proceed) {

        data.token = uuid();

        return proceed();
    }
}